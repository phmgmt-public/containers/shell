import os

CA_FILE = '/etc/ssl/certs/ca-certificates.crt'

UPGRADE_CHECK_ENABLED = False
CHECK_EMAIL_DELIVERABILITY = False

SERVER_MODE = True

DEFAULT_SERVER = '127.0.0.1'
DEFAULT_SERVER_PORT = 5050

DATA_DIR = '/opt/pgadmin4/var'
STORAGE_DIR = os.path.join(DATA_DIR, 'storage')
SQLITE_PATH = os.path.join(DATA_DIR, 'config', 'pgadmin4.db')

SESSION_DB_PATH = '/dev/shm/pgadmin4_session'

DEFAULT_BINARY_PATHS = {
  'pg': '/usr/bin',
}
