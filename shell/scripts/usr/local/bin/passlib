#!/usr/bin/env python3
#
# Usage: passlib <method> <password>
#
# All `passlib`-supported methods are supported <https://passlib.readthedocs.io/>.
# In addition, an `ebcrypt` method is supported which is compatible with BCrypt.Net.

import base64
import hashlib
import sys

import passlib.hash

def main():
    if len(sys.argv) < 3:
        # TODO: add `--list` to list supported methods
        # TODO: add `--verify <hash>`
        # TODO: add ability to add `using()` parameters
        print(f'usage: {sys.argv[0]} <method> <password>')
        return 1

    method = sys.argv[1]
    passwd = sys.argv[2].encode('utf-8')

    if method == 'ebcrypt':
        from passlib.hash import bcrypt

        prehash = base64.b64encode(hashlib.sha384(passwd).digest())
        print(bcrypt.hash(prehash))
    else:
        try:
            hasher = getattr(passlib.hash, method)
        except AttributeError:
            print(f'unsupported method {method!r}')
            return 1

        print(hasher.hash(passwd))

if __name__ == '__main__':
    sys.exit(main())

# vim: set sw=4:
