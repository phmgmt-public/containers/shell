#!/bin/sh

if [ ! -d "/root/compose" ]; then
  echo "This service can only be used with the Docker Compose."
  read
  exit 1
fi

set -m

cd /root/compose/ph-platform

while true; do
    SERVICES=`docker-compose ps --services | sort | sed 's@ *$@ - no@'`

    CHOICES=`dialog \
      --output-fd 1 \
      --backtitle "Docker Compose Logs" \
      --title "Select Service(s)" \
      --clear \
      --ok-label "Tail" \
      --extra-button \
      --extra-label "Full" \
      --checklist "(selecting no services will watch all logs)" \
      0 60 0 \
      $SERVICES`

    RESULT="$?"
    TAIL=""

    clear

    if [ "$RESULT" = "0" ]; then
        TAIL="--tail=10"
    elif [ "$RESULT" != "3" ]; then
        exit 1
    fi

    docker-compose logs -f $TAIL $CHOICES &
    trap '' SIGINT
    fg %%
    trap - SIGINT
done
